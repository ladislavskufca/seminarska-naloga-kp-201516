/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	rest: function (req, res) {
        //kak je header v req?
        var header = req.get('Content-Type');
        if (header == "application/json" || header == "text/html") {
            //vrni json

            var katerID = req.query.id;

            User.find()
            .where({id: katerID})
            .exec(function (err, response) {
                return res.send(response[0]);  
            });

        }
        else if (header == "application/xml") {
            //vrni xml
            
            var katerID = req.query.id;

            User.find()
            .where({id: katerID})
            .exec(function (err, response) {
                
                //https://www.npmjs.com/package/js2xmlparser
                var js2xmlparser = require("js2xmlparser");

                var data = response[0];   
                var rez = js2xmlparser("user", data);   
                //return res.send(rez);  
                return res.header('Content-Type','application/xml').send(rez);
            });
            
        }
        else if (header == "text/plain") {
            //vrni plain text

            var katerID = req.query.id;

            User.find()
            .where({id: katerID})
            .exec(function (err, response) {
                var tmp = response[0];
                var rez = tmp.email + " " + tmp.geslo + " " + tmp.uporabnisko_ime + " " + tmp.id + " " + tmp.createdAt + " " + tmp.updatedAt;
                return res.header('Content-Type','text/plain').send(rez);
            });
        }
        
    }
};

