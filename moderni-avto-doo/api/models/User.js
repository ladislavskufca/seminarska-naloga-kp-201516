/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
    email: {
        type: 'email',
        required: true,
    },
    geslo: {
        type: 'string',
        minLength: 5,
    },
    uporabnisko_ime: {
        type: 'string',
        size: 20,
    },
    cars: {
        collection: 'car',
        via: 'lastnik'
    }
  }
};