/**
* Car.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {
  attributes: {
  	znamka: {
  		type: 'string',
  		size: 25,
  	},
  	model: {
  		type: 'string',
  		size: 25,
  	},  
  	prostornina: {
  		type: 'float',
  	}, 	
  	gorivo: {
  		type: 'string',
  		size: 15,
  	},
  	letnik: {
  		type: 'int',
  	},
    lastnik: {
      model: 'user'
    }
  }
};


